<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Tarea</title>
</head>
<body>
    
</body>
</html>

<?php

//Crea un array llamado nombres que contenga varios nombres

$nombres = array(
    "José",
    "Marcos",
    "Mario",
    "Juan",
    "Alberto",
);

print_r($nombres);

//Muestra el número de elementos que tiene el array (función count)

echo "___________";

print_r(count($nombres)."\n");

//Crea una cadena que contenga los nombres de los alumnos existentes en el array separados por un espacio y muéstrala (función de strings implode)

echo "___________";

print_r(implode($nombres)."\n");

//Muestra el array ordenado alfabéticamente (función asort).

echo "___________";

asort($nombres);
print_r($nombres);

//Muestra por pantalla las diferencias con ksort y con sort.

echo "___________";

ksort($nombres);
foreach ($nombres as $key => $val) {
    echo "$key = $val\n";
}

echo "___________";

sort($nombres);
foreach ($nombres as $key => $val) {
    echo "$key = $val\n";
}


//Muestra el array en el orden inverso al que se creó (función array_reverse)

echo "___________";

array_reverse($nombres);
print_r($nombres);


//Muestra la posición que tiene tu nombre en el array (función array_search)

echo "___________";

print_r(array_search("José", $nombres));

//Crea un array de alumnos donde cada elemento sea otro array que contenga el id, nombre y edad del alumno.


$nombres1 = array("Miguel", "Carlos", "Marta");

$alumnos = array();
    for($i = 0; $i < count($nombres1); $i++){
        $edad = rand(4,18);
        $alumnos[$i] = array("Id" => $i, "nombre" => $nombres1[$i], "Edad" => $edad);

        echo "___________";
    }

    //print_r($alumnos);


?>

//Crea una tabla html en la que se muestren todos los datos de los alumnos.  
<table border ="1">
    <tr>
        <th>Id</th>
        <th>Nombre</th>
        <th>Edad</th>
    </tr>
    <?php
    foreach ($alumnos as $r){
    ?>
    <tr>
        <?php
        foreach ($r as $v){
        ?>
            <td><?php echo$v;?></td>
        <?php
            }
        ?>
    </tr>
    <?php
    }
    ?>
</table>

<?php

//Utiliza la función array_column para obtener un array indexado que contenga únicamente los nombres de los alumnos y muéstralo por pantalla.

echo "___________";

$nombres2 = array_column($alumnos, "nombre");
    print_r($nombres2);


//Crea un array con 10 números y utiliza la función array_sum para obtener la suma de los 10 números.

echo "___________";
$numeros = array( 1,2,3,4,5,6,7,8,9,10);
echo "El resultado es: ". array_sum($numeros)."\n";
?>