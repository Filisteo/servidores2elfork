<?php

//El programa debe estar protegido contra ataques XSS (función htmlspecialchars).
//Al aplicar la función a todos los echos, las etiquetas html quedan inservibles
//Aplique directamente la función htmlspecialchars cuando ejecuta el metodo GET
function html($string){
    htmlspecialchars($string, ENT_QUOTES, "UTF-8");
}

    $url = "http://192.168.56.101/tarea2.php/?guess=";

    echo "<title>Gabriel</title>";

    echo "<h1>Guess my number</h1>";

    $guessin = htmlspecialchars($_GET['guess'],ENT_QUOTES, "UTF-8");

    echo "<p>Número de busqueda: $guessin</p>";

    $number = 50;

    //echo "<p>Número de que he pensado es: $number</p>";
    

    if($guessin==''){
        echo "No has especificado ningún número";
    } elseif(is_numeric($guessin)===FALSE) {
        echo "Debes introducir un número";
    } elseif($guessin > $number){
        echo "Mi número es menor";
    }elseif($guessin < $number){
        echo "Mi número es mayor";
    }elseif($guessin == $number){
        echo "Enhorabuena. Has acertado";
    }

    
?>