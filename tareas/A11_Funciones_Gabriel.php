<?php 
declare(strict_types=1);

//Ejercicio 1
    //El constructor declare es usado para fijar directivas de ejecución para un bloque de código.
    //Fuerza a que los métodos acepten varaibles únicamente
    //del tipo exacto que se declaran.
    /*
    function outputAnInteger(int $num)
    {
        echo $num;
    }
    outputAnInteger(3);


    echo "<br>";
    */
    //Indicando el tipo de dato que va a ser el argumento de una función
    //utilizando un return e imprimiendolo con var_dump()

    
        /*
        function sum($a, $b): float {
            return $a + $b;
        }
        
        var_dump(sum(1, 2));
    
        echo"<br>";
        */
    //devuelve el tipo de dato y la operación resuelta.

   //return null, return string como una posibilidad y en una función podemos obligar 
   //a que, el argumento sea de un tipo declarado o la respuesta sea
   //de un tipo en concreto
    
   /*
    function get_item(): ?string {
        if (isset($_GET['item'])) {
            return $_GET['item'];
        } else {
            return null;
        }
    }*/

//Ejercicio 2
echo"<br>";



    function insert($nombre_tabla,$datos):string {
        $order = implode(", ",array_keys($datos));
        $order2 = implode(":",array_keys($datos));
        $frase =  "insert into ".$nombre_tabla."(".$order.") values (".$order2.")";
        return $frase;
    }

    echo insert('alumnos',['id'=> 1, 'nombre' => 'Jesús', 'curso' => '2º DAW']);

    //.array_keys($datos)." values ".$datos;
    //['id'=> 1, 'nombre' => 'Jesús', 'curso' => '2º DAW']

//Ejercicio 3
echo "<br>";

function insert2($nombre_tabla,$datos,&$cadena) {
    $order = implode(", ",array_keys($datos));
    $order2 = implode(":",array_keys($datos));
    $cadena =  "insert into ".$nombre_tabla."(".$order.") values (".$order2.")";
}

$cadena = "insert into tabla (campos) values (valores)";
echo insert2('alumnos',['id'=> 1, 'nombre' => 'Jesús', 'curso' => '2º DAW'], $cadena);

echo $cadena;
?>
